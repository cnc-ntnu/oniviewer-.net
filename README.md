# OniViewer .NET #

Viewer for ONI files recorded with OpenNI 2. Allows to view individual streams. Quick and dirty, built with .Net.

![oniviewer.jpg](https://bitbucket.org/repo/AnkR89/images/3668919806-oniviewer.jpg)

### How do I get set up? ###

In order to build the solution you will need [NiWrapper.Net](https://github.com/falahati/NiWrapper.Net). Get it first!

* Open the solution in Visual Studio and build it.
* To actually run the app the following should be in the same folder as your exe:
    * OpenNI2 folder with drivers
    * NiWrapper.dll
    * OpenNI2.dll
    
### Download ###

There is a binary package in [Downloads](https://bitbucket.org/cnc-ntnu/oniviewer-.net/downloads/) section.

### License ###

Copyright (C) 2014 Vadim Frolov - vadim.frolov@ntnu.no

This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License.txt, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.