﻿namespace OniViewer.Net
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb_color = new System.Windows.Forms.PictureBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.edFilePath = new System.Windows.Forms.TextBox();
            this.cbColor = new System.Windows.Forms.CheckBox();
            this.cbDepth = new System.Windows.Forms.CheckBox();
            this.cbInfrared = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.pb_depth = new System.Windows.Forms.PictureBox();
            this.pb_ir = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_depth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ir)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_color
            // 
            this.pb_color.BackColor = System.Drawing.Color.Black;
            this.pb_color.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pb_color.ErrorImage = null;
            this.pb_color.InitialImage = null;
            this.pb_color.Location = new System.Drawing.Point(656, 24);
            this.pb_color.Name = "pb_color";
            this.pb_color.Size = new System.Drawing.Size(316, 273);
            this.pb_color.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_color.TabIndex = 16;
            this.pb_color.TabStop = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBrowse.Location = new System.Drawing.Point(498, 340);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 24;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 326);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "File to view:";
            // 
            // edFilePath
            // 
            this.edFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.edFilePath.Location = new System.Drawing.Point(12, 342);
            this.edFilePath.Name = "edFilePath";
            this.edFilePath.Size = new System.Drawing.Size(480, 20);
            this.edFilePath.TabIndex = 22;
            this.edFilePath.TextChanged += new System.EventHandler(this.edFilePath_TextChanged);
            // 
            // cbColor
            // 
            this.cbColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbColor.AutoSize = true;
            this.cbColor.Location = new System.Drawing.Point(198, 383);
            this.cbColor.Name = "cbColor";
            this.cbColor.Size = new System.Drawing.Size(50, 17);
            this.cbColor.TabIndex = 28;
            this.cbColor.Text = "Color";
            this.cbColor.UseVisualStyleBackColor = true;
            // 
            // cbDepth
            // 
            this.cbDepth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbDepth.AutoSize = true;
            this.cbDepth.Location = new System.Drawing.Point(137, 383);
            this.cbDepth.Name = "cbDepth";
            this.cbDepth.Size = new System.Drawing.Size(55, 17);
            this.cbDepth.TabIndex = 27;
            this.cbDepth.Text = "Depth";
            this.cbDepth.UseVisualStyleBackColor = true;
            // 
            // cbInfrared
            // 
            this.cbInfrared.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbInfrared.AutoSize = true;
            this.cbInfrared.Location = new System.Drawing.Point(69, 383);
            this.cbInfrared.Name = "cbInfrared";
            this.cbInfrared.Size = new System.Drawing.Size(62, 17);
            this.cbInfrared.TabIndex = 26;
            this.cbInfrared.Text = "Infrared";
            this.cbInfrared.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 384);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Sensors:";
            // 
            // btnPlay
            // 
            this.btnPlay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPlay.Location = new System.Drawing.Point(342, 379);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(69, 23);
            this.btnPlay.TabIndex = 31;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoad.Location = new System.Drawing.Point(261, 379);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(69, 23);
            this.btnLoad.TabIndex = 32;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // pb_depth
            // 
            this.pb_depth.BackColor = System.Drawing.Color.Black;
            this.pb_depth.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pb_depth.ErrorImage = null;
            this.pb_depth.InitialImage = null;
            this.pb_depth.Location = new System.Drawing.Point(334, 24);
            this.pb_depth.Name = "pb_depth";
            this.pb_depth.Size = new System.Drawing.Size(316, 273);
            this.pb_depth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_depth.TabIndex = 16;
            this.pb_depth.TabStop = false;
            // 
            // pb_ir
            // 
            this.pb_ir.BackColor = System.Drawing.Color.Black;
            this.pb_ir.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pb_ir.ErrorImage = null;
            this.pb_ir.InitialImage = null;
            this.pb_ir.Location = new System.Drawing.Point(12, 24);
            this.pb_ir.Name = "pb_ir";
            this.pb_ir.Size = new System.Drawing.Size(316, 273);
            this.pb_ir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_ir.TabIndex = 16;
            this.pb_ir.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Infrared";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(331, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Depth";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(656, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "Color";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 421);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pb_color);
            this.Controls.Add(this.pb_depth);
            this.Controls.Add(this.pb_ir);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.cbColor);
            this.Controls.Add(this.cbDepth);
            this.Controls.Add(this.cbInfrared);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edFilePath);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMainFormClosing);
            this.Load += new System.EventHandler(this.FrmMainLoad);
            ((System.ComponentModel.ISupportInitialize)(this.pb_color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_depth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_color;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edFilePath;
        private System.Windows.Forms.CheckBox cbColor;
        private System.Windows.Forms.CheckBox cbDepth;
        private System.Windows.Forms.CheckBox cbInfrared;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.PictureBox pb_depth;
        private System.Windows.Forms.PictureBox pb_ir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

