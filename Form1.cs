﻿using System;
using System.Drawing;
using System.Windows.Forms;

using OpenNIWrapper;
using System.IO;

namespace OniViewer.Net
{
    public partial class Form1 : Form
    {
        private String _lastDir;
        private String _oniName;

        private Device _currentDevice;
        private VideoStream _irStream;
        private VideoStream _depthStream;
        private VideoStream _colorStream;

        private Bitmap _irBitmap;
        private Bitmap _depthBitmap;
        private Bitmap _colorBitmap;

        private bool _isPlaying;

        public Form1()
        {
            InitializeComponent();
            _oniName = "./captured.oni";

            _irBitmap = new Bitmap(1, 1);
            _colorBitmap = new Bitmap(1, 1);
            _depthBitmap = new Bitmap(1, 1);

            _isPlaying = false;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!_isPlaying)
                return;

            if (cbColor.Enabled && cbColor.Checked)
                PaintColor(e);

            if (cbDepth.Enabled && cbDepth.Checked)
                PaintDepth(e);

            if (cbInfrared.Enabled && cbInfrared.Checked)
                PaintIr(e);
        }

        private void PaintColor(PaintEventArgs e)
        {
            if (pb_color.Visible)
            {
                pb_color.Visible = false;
            }

            if (_colorBitmap == null)
            {
                return;
            }

            lock (_colorBitmap)
            {
                // OnPaint happens on UI Thread so it is better to always keep this lock in place
                var canvasSize = pb_color.Size; // Even though we don't use PictureBox, we use it as a placeholder
                var canvasPosition = pb_color.Location;

                var ratioX = canvasSize.Width / (double)_colorBitmap.Width;
                var ratioY = canvasSize.Height / (double)_colorBitmap.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var drawWidth = Convert.ToInt32(_colorBitmap.Width * ratio);
                var drawHeight = Convert.ToInt32(_colorBitmap.Height * ratio);

                var drawX = canvasPosition.X + Convert.ToInt32((canvasSize.Width - drawWidth) / 2);
                var drawY = canvasPosition.Y + Convert.ToInt32((canvasSize.Height - drawHeight) / 2);

                e.Graphics.DrawImage(_colorBitmap, drawX, drawY, drawWidth, drawHeight);

                /////////////////////// If we do create a new Bitmap object per each frame we must
                /////////////////////// make sure to DISPOSE it after using.
                // bitmap.Dispose();
                /////////////////////// END NOTE
            }
        }

        private void PaintDepth(PaintEventArgs e)
        {
            if (pb_depth.Visible)
            {
                pb_depth.Visible = false;
            }

            if (_depthBitmap == null)
            {
                return;
            }

            lock (_depthBitmap)
            {
                // OnPaint happens on UI Thread so it is better to always keep this lock in place
                var canvasSize = pb_depth.Size; // Even though we don't use PictureBox, we use it as a placeholder
                var canvasPosition = pb_depth.Location;

                var ratioX = canvasSize.Width / (double)_depthBitmap.Width;
                var ratioY = canvasSize.Height / (double)_depthBitmap.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var drawWidth = Convert.ToInt32(_depthBitmap.Width * ratio);
                var drawHeight = Convert.ToInt32(_depthBitmap.Height * ratio);

                var drawX = canvasPosition.X + Convert.ToInt32((canvasSize.Width - drawWidth) / 2);
                var drawY = canvasPosition.Y + Convert.ToInt32((canvasSize.Height - drawHeight) / 2);

                e.Graphics.DrawImage(_depthBitmap, drawX, drawY, drawWidth, drawHeight);

                /////////////////////// If we do create a new Bitmap object per each frame we must
                /////////////////////// make sure to DISPOSE it after using.
                // bitmap.Dispose();
                /////////////////////// END NOTE
            }
        }

        private void PaintIr(PaintEventArgs e)
        {
            if (pb_ir.Visible)
            {
                pb_ir.Visible = false;
            }

            if (_irBitmap == null)
            {
                return;
            }

            lock (_irBitmap)
            {
                // OnPaint happens on UI Thread so it is better to always keep this lock in place
                var canvasSize = pb_ir.Size; // Even though we don't use PictureBox, we use it as a placeholder
                var canvasPosition = pb_ir.Location;

                var ratioX = canvasSize.Width / (double)_irBitmap.Width;
                var ratioY = canvasSize.Height / (double)_irBitmap.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var drawWidth = Convert.ToInt32(_irBitmap.Width * ratio);
                var drawHeight = Convert.ToInt32(_irBitmap.Height * ratio);

                var drawX = canvasPosition.X + Convert.ToInt32((canvasSize.Width - drawWidth) / 2);
                var drawY = canvasPosition.Y + Convert.ToInt32((canvasSize.Height - drawHeight) / 2);

                e.Graphics.DrawImage(_irBitmap, drawX, drawY, drawWidth, drawHeight);

                /////////////////////// If we do create a new Bitmap object per each frame we must
                /////////////////////// make sure to DISPOSE it after using.
                // bitmap.Dispose();
                /////////////////////// END NOTE
            }
        }

        public static void HandleError(OpenNI.Status status)
        {
            if (status == OpenNI.Status.Ok)
            {
                return;
            }

            MessageBox.Show(
                string.Format(@"Error: {0} - {1}", status, OpenNI.LastError),
                @"Error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Asterisk);
        }

        private void FrmMainFormClosing(object sender, FormClosingEventArgs e)
        {
            OpenNI.Shutdown();
        }

        private void FrmMainLoad(object sender, EventArgs e)
        {
            HandleError(OpenNI.Initialize());
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // Create an instance of the open file dialog box.
            var openFileDialog1 = new OpenFileDialog
            {
                Filter = @"ONI files (*.oni)|*.oni",
                FilterIndex = 1,
                Multiselect = false,
                CheckFileExists = true,
                InitialDirectory = _lastDir,
                RestoreDirectory = true,
                FileName = Path.GetFileName(_oniName)
            };

            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            _oniName = openFileDialog1.FileName;
            edFilePath.Text = _oniName;
            _lastDir = Path.GetDirectoryName(_oniName);
        }

        private void edFilePath_TextChanged(object sender, EventArgs e)
        {
            _oniName = edFilePath.Text;
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            //DeviceInfo info;
            //info.Uri = _oniName;

            if (_isPlaying)
            {
                if (_colorStream != null && _colorStream.IsValid)
                {
                    _colorStream.Stop();
                    _colorStream.OnNewFrame -= ColorOnNewFrame;
                }
                if (_depthStream != null && _depthStream.IsValid)
                {
                    _depthStream.Stop();
                    _depthStream.OnNewFrame -= DepthOnNewFrame;
                }
                if (_irStream != null && _irStream.IsValid)
                {
                    _irStream.Stop();
                    _irStream.OnNewFrame -= IrOnNewFrame;
                }

                btnPlay.Text = @"Play";
                btnLoad.Enabled = true;
                btnBrowse.Enabled = true;
                edFilePath.Enabled = true;

                _isPlaying = false;
            }
            else
            {
                if (cbColor.Enabled && cbColor.Checked && _colorStream != null)
                {
                    _colorStream.OnNewFrame += ColorOnNewFrame;
                    _colorStream.Start();
                }
                if (cbDepth.Enabled && cbDepth.Checked && _depthStream != null)
                {
                    _depthStream.OnNewFrame += DepthOnNewFrame;
                    _depthStream.Start();
                }

                if (cbInfrared.Enabled && cbInfrared.Checked && _irStream != null)
                {
                    _irStream.OnNewFrame += IrOnNewFrame;
                    _irStream.Start();
                }

                btnPlay.Text = @"Stop";
                btnLoad.Enabled = false;
                btnBrowse.Enabled = false;
                edFilePath.Enabled = false;

                _isPlaying = true;
            }
        }

        private void IrOnNewFrame(VideoStream videoStream)
        {
            if (!videoStream.IsValid || !videoStream.IsFrameAvailable())
                return;

            using (VideoFrameRef frame = videoStream.ReadFrame())
            {
                if (!frame.IsValid)
                    return;

                const VideoFrameRef.CopyBitmapOptions options = VideoFrameRef.CopyBitmapOptions.Force24BitRgb
                                                            | VideoFrameRef.CopyBitmapOptions.DepthFillShadow;
                lock (_irBitmap)
                {
                    try
                    {
                        frame.UpdateBitmap(_irBitmap, options);
                    }
                    catch (Exception)
                    {
                        // Happens when our Bitmap object is not compatible with returned Frame
                        _irBitmap = frame.ToBitmap(options);
                    }
                }
                if (!pb_ir.Visible)
                {
                    Invalidate();
                }
            }
        }

        private void DepthOnNewFrame(VideoStream videoStream)
        {
            if (!videoStream.IsValid || !videoStream.IsFrameAvailable())
                return;

            using (VideoFrameRef frame = videoStream.ReadFrame())
            {
                if (!frame.IsValid)
                    return;

                const VideoFrameRef.CopyBitmapOptions options = VideoFrameRef.CopyBitmapOptions.Force24BitRgb
                                                            | VideoFrameRef.CopyBitmapOptions.DepthFillShadow;
                lock (_depthBitmap)
                {
                    try
                    {
                        frame.UpdateBitmap(_depthBitmap, options);
                    }
                    catch (Exception)
                    {
                        // Happens when our Bitmap object is not compatible with returned Frame
                        _depthBitmap = frame.ToBitmap(options);
                    }
                }
                if (!pb_depth.Visible)
                {
                    Invalidate();
                }
            }
        }

        private void ColorOnNewFrame(VideoStream videoStream)
        {
            if (!videoStream.IsValid || !videoStream.IsFrameAvailable())
                return;

            using (VideoFrameRef frame = videoStream.ReadFrame())
            {
                if (!frame.IsValid)
                    return;

                const VideoFrameRef.CopyBitmapOptions options = VideoFrameRef.CopyBitmapOptions.Force24BitRgb
                                                            | VideoFrameRef.CopyBitmapOptions.DepthFillShadow;
                lock (_colorBitmap)
                {
                    try
                    {
                        frame.UpdateBitmap(_colorBitmap, options);
                    }
                    catch (Exception)
                    {
                        // Happens when our Bitmap object is not compatible with returned Frame
                        _colorBitmap = frame.ToBitmap(options);
                    }
                }
                if (!pb_color.Visible)
                {
                    Invalidate();
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (_currentDevice != null)
            {
                _currentDevice.Close();
                _colorStream = null;
                _depthStream = null;
                _irStream = null;
            }

            _currentDevice = Device.Open(_oniName);

            var haveColor = _currentDevice.HasSensor(Device.SensorType.Color);
            cbColor.Enabled = haveColor;
            cbColor.Checked = haveColor;
            if (haveColor)
            {
                _colorStream = _currentDevice.CreateVideoStream(Device.SensorType.Color);
            }

            var haveIr = _currentDevice.HasSensor(Device.SensorType.Ir);
            cbInfrared.Enabled = haveIr;
            cbInfrared.Checked = haveIr;
            if (haveIr)
            {
                _irStream = _currentDevice.CreateVideoStream(Device.SensorType.Ir);
            }

            var haveDepth = _currentDevice.HasSensor(Device.SensorType.Depth);
            cbDepth.Enabled = haveDepth;
            cbDepth.Checked = haveDepth;
            if (haveDepth)
            {
                _depthStream = _currentDevice.CreateVideoStream(Device.SensorType.Depth);
            }
        }


    }
}
